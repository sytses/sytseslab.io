---
layout: post
title:  "Digital Music"
date:   2004-01-13
---

This essay was also published in a book about MP3 and the digital music revolution by John Hedtke. Distributed by Top Floor Publishing with ISBN 0-9661032-4-6.

The internet is changing the world, and it doing that by taking down the walls that make up our normal society. Normally an artist has to give away his copyrights to get his music published and people had to sign Non Disclosure Agreements and pay cash to see the source of an OS. But thanks to the internet that's changing, everybody can set up a homepage a publish their mp3's and with Linux everybody can modify and distribute their own OS.

This scares the hell out of now days establishment and they try to stop it. The Rcaa trying to stop mp3 every way they can and Microsoft's halloween documents show they are really scared about opensource. But the injunction of the Rio and the modifying of protocols won't make the walls come back, intellectual freedom is here to stay.

Of course this freedom comes with some drawbacks, most notably the copying of illegal mp3's. Because of this the Rcaa started development of SDMI, a standard that would make it harder (but not impossible) to copy digital music. But this standard would also be much less flexible then mp3 (sharing it with friends, copying it to your car) and it would prevent little artists from distributing their music without a big record label. And that is what it's all about, the six big labels don't want an equal playing field because it would undermine their hold on the market.

But that's also why it shall fail, people want to be able to share their music, play it in their car or on their walkman. And little artists will continue to give away their music on mp3.com and other sites. Not only because the want their music to be heard from an artist point of view but also with a business sense. Yes people, money can be made by giving your music away free. Just like Redhat makes money by giving their software away new record labels will make money by giving their software away. And artist will make money with advertising, concerts, T-shirts, limited edition songs, poems, fanclubs, email lists, commercials and many other creative ways.

When the dust settles down the big six record labels will have a lot more competition, from goodnoise and other digital labels and from the independent labels which finally have means of distribution. People will be able to access their music anytime, anywhere with minimal cost but great flexibility and service. Artist will retain their copyrights but will still have to work hard to get their music heard. But as a whole artists will earn more money while at the same time people will send less. Why is that? Well, it's due to one of the oldest business tactics, to cut out the middleman. Lawyers, cd-shops, cd-makers, distribution companies and all other people that higher the price of a cd; be afraid, be very afraid.
