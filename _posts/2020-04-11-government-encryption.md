---
layout: post
title:  "Government encryption"
date:   2020-04-11
---

In the current debate over decryption by governments there are three basic postions:

1. Government backdoor: the government should be able to see everything.
1. Vendor encryption: the vendor should be able to see everything.
1. End-to-end encryption: nobody but the users should be able to see everything.

The current debate is centered on the [EARN IT Act](https://www.schneier.com/blog/archives/2020/03/the_earn-it_act.html) but there have been [many debates before it](https://en.wikipedia.org/wiki/Crypto_Wars).

A simple version of the arguments against the postions is:

1. Government backdoor will allow the government to do everything they want without controls.
1. Vendor encryption will allow companies to do everything they want without controls.
1. End-to-end encryption will make it harder to prevent crime.

The above is a simplified version and there are many considerations, from human rights, to terrorism, to the increasing availibility of meta-data.
This is a complex subject and I'm not sure what the best solution is.
I do want to bring awareness to a fourth option that I have not seen discussed much, government encryption.
I **don't know** if government encryption is a good solution to the problem!

Government encryption means that every piece of content is encrypted in parallel in the two following ways:

1. Once with end-to-end encryption for use in normal operation where only the user can see the content.
1. Once with a government public key for use when there is a [search warrant](https://en.wikipedia.org/wiki/Search_warrant).

Each private key (the user owned one and the govenment owned one) is enough to decrypt the message by itself, this is not [multiple encryption](https://en.wikipedia.org/wiki/Multiple_encryption).

If there is a search warrent against the user:

1. The vendor checks that search warrent presented by the government is valid.
1. The vendor retrieves the govenment encoded ciphertext and sends it to the govenment
1. The government decrypts the ciphertext with their private key.

The advantages of government encryption are:

1. Government access is double-checked by vendor who can now make a [transparency report](https://en.wikipedia.org/wiki/Transparency_report).
1. The vendor will not be able to access any user content.
1. When there is a valid search warrent the data can be accessed to stop crime.

The disadvantages of government encryption are:

1. The government needs to go through an extra step to get the data compared to a backdoor, delaying access to potentially crucial information.
1. The usability of end-to-end encryption is worse than vendor encryption since key management is harder.
1. There is complexity in encrypting messages twice leading to a higher cost to develop and run software.
1. The government encryption introduces another attack vector, it is less safe than only using end-to-end encryption.
