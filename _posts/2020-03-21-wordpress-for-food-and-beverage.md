---
layout: post
title:  "Wordpress for food and beverage"
date:   2020-03-21
---

The COVID-19 pandemic is a [huge problem for the food and beverage industry](https://www.opentable.com/state-of-industry).

Right now a lot of restaurants and bars have to close except for takeout.

When they can open up again there will still be risk of spreading the virus. We're making an application to reduce that risk so they can open up sooner.

In an establishment a virus can be spread by:

1. Patrons to eachother, we can minimize this by distancing the patrons from eachother so at most they only are infected by and will infect people in their party.
1. Back of the house personel, they can't be infected by patrons but through food, dishes, glasses, and cutlery can infect others.
1. Waiting staff, they can be infected by patrons and they can spread it to patrons.

The waiting staff is the link with the higest risk because they can be infected by a lot of people and they can spread it to a lot of people. So to minimize risk we need to limit their interactions.

Waiting staff has four main tasks:

1. Taking orders
1. Serving food
1. Cleaning up
1. Payment

We'll make an application that can take orders and do payment.

This is already popular in China using [WeChat ordering](https://sampi.co/wechat-app-restaurant-business/): “WeChat app restaurant ordering system also eliminates the need for bill splitting: everyone can simply order and pay for their food directly with their own WeChat Pay.” and “Statistics on sales, profit analysis on specific items and various CRM functions.“. WeChat [also allows for](https://walkthechat.com/10-wechat-food-beverage-industry-case-studies/): delivery, loyalty, waitlist, and promotions. In the west I've mostly seen it at airport restaurants that use terminals. Terminals are an infection hazard so we'll allow people to use their own phone.

The way it works is:

1. Patrons scan a QR code on their table
1. They select food and beverages
1. They pay
1. The waiter brings the food and beverages

Please note that everyone orders and pays for themselves, so you don't have to split the bill.

The advantages for patrons are:

1. Lower risk of infection (no menu to touch, waiter can keep distance, no touching credit cards)
1. No need to get the attention of the waiting staff
1. Clean and up to date menu on your phone
1. Ease of payment (can use Apple Pay and Google Pay)

The advantages for the establishment are:

1. Lower risk of infection (can open sooner, lower risk to employees)
1. Quicker service (higher table turnover)
1. Don't need to print new menu
1. Every order is paid (no dine and dash)

We'll make this open source since it will be needed by a lot of organizations but at the same time it will need to sell itself since the food and beverage industry is notoriously hard to sell to because:

1. It is a low margin industry so subscription prices are low to zero and transaction markup is below 1%.
1. Technical solution don't save a lot of money since waiter sometimes earn $2.13 per hour before tips.
1. If there is an existing process and/or Point of Sale (PoS) system people are unlikely to change it.
1. Restaurants don't have great success inputting their menu, most delivery services do the initial conversion for them.
1. If some portion of customers don't order via the application it can't be used exclusively, leading to confusion.
1. The application is overhead if people are sitting at the bar already.
1. There are many small organizations making sales more expensive.
1. The app is most useful during peak times, making for two ways of working.
1. The app is most useful during the summertime when there are terraces, in many areas this is a limited time of the year.

Potential problems with the application are:

1. Asking questions about the menu (need to be aware of allergies and dietary restrictions, although online delivery services have been able to acommodate this)
1. You lose some of the hospotality (although waiters should have more time to ask if everything is well)
1. People will have a harder time putting their phones down since they need them to order
1. Transaction costs are higher if multiple people pay multiple rounds compared to one check after all rounds
1. If people are already ordering at the bar without the app they can take drinks with them already so waiters only have to deliver the food

To make it simple to adopt this application it will be:

1. Open source (MIT license)
1. Also offer a SaaS version (like Wordpress.com)
1. Written in Ruby on Rails (since I'm familiar with that)
1. Mobile only (mobile and tablets)
1. Simple authentication (should be easy to use)
1. Start out very basic (we have to make it for free)

If you are interested 

The project will be called Bark, see the [readme](https://gitlab.com/sytses/bark/-/blob/master/README.md).

If you are interested please fill out our [contact form](https://docs.google.com/forms/d/e/1FAIpQLScw2Lujb6ZypgCy_7uDjaWfX_1KtHxBvccQLOodxKPG8aeFMA/viewform).

If you see anything wrong in this blog post feel free to suggest changes by editing [this post](https://gitlab.com/sytses/sytses.gitlab.io/-/blob/master/_posts/2020-03-21-wordpress-for-food-and-beverage.md).
