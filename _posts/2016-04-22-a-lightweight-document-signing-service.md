---
layout: post
title:  "A lightweight document signing service"
date:   2016-04-22
---

## Introduction

I love the convenience of [Hellosign](https://www.hellosign.com/). But it still feels a bit cumbersome, like sending files before [WeTransfer](https://www.wetransfer.com/). If you are opinionated and assume people use Google Docs you can get a simpler workflow. I outline this idea below, please use it and make this service.

## Workflow

1. Go to website of this service and sign in with your Google Account (if you are not already signed in from a previous session).
1. The service suggests make a contract out of the latest Google Doc that you viewed (this Google Doc should not contain any signature blocks).
1. Paste email addresses of the people that need to sign it.
1. When you submit those emails every participant will get an email to sign the document.

## Features

1. It automatically generates signature blocks
1. It tries to prefill the names of the people that need to sign based on their email addresses.
1. When someone signs, it automatically adds a date and tries to guess your location.
1. If you update the Google Doc the contract is updated automatically and the signing process is restarted.
1. If the contract is updated and the signing process restarted it will show you the changes to the previous document your reviewed or signed.
1. There is no need for you to make signature blocks, saving you from a tedious alignment process.
1. If you need to see overview of contracts that you email address is involved with you can do so since you're signed in to your Google account.
1. You can allow for a file picker from your Google Drive to select another Google Doc that the one you viewed last.
1. You can make this into a Google Docs add-on.
1. There can be an optional field for email addresses of observers.
1. If you pay a moderate fee the contract hash will be registered on the Bitcoin blockchain.
1. If you navigate to the site from an existing Google document it will skip step 1.
1. If your Google Document contains email addresses at the top prefix with 'signees' it will suggest those as people who will sign.
1. If you combine the last two things you can create a bookmark that will send out the contract by just clicking