---
layout: post
title:  "The 3 best traffic rules in both the United States and Europe"
date:   2018-01-14
---

## Introduction

Since moving from Europe to the United States 3 years ago I compared their rules.
Below are the things I think other countries should copy.

## United States highlights

1. Passing right on the freeway => Solves the nr. 1 irritation of EU drivers, cars staying left too long.
1. Having the traffic lights at the oppositive end of the intersections. => Much more natural position, easier to spot, and no more leaning forward.
1. Allowing you to turn right at a red light when it is possible. => No more waiting for no reason.

## European highlights

1. It is always clear if you have right of way or an all stop intersections. => US has 2-way and 4-way signs below the stop sign but not always.
1. Higher allowed steed on the freeway of 130km/h => US is mostly 65 m/h, sometimes 70, but never the equivalent 80.
1. Make sure there is space before indicating. => In the US you indicate first which means you're not sure if the car saw you.
