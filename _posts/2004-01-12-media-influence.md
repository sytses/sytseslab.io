---
layout: post
title:  "Media Influence"
date:   2004-01-12
---

We have much more choices these days.
We can learn anything we want.
We can have friends all over the world.
We can say whatever we want.

Indeed this is partly true.
I can have another profession as my father.
I can have a friend at the other side of the Atlantic.
I can say what I want on my webpage.

But there is a stronger current below at this.
That dictates how I should look.
That dictates who my friends should be.
That dictates what I should do with my live.

This current comes from the media.
It's in beauty magazines.
It's in films, music and all that's around us.
It's in the minds of people all around us.

Such a thing isn't bad in itself.
We know how to make quicker choices.
We know what is expected of us.
We know what to do when we don't have a clue.
