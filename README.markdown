This is the content of [sytse.com](https://sytse.com/).

The [canonical source](https://gitlab.com/sytses/sytses.gitlab.io) is on GitLab.com.

Hosted with Jekyll, Kramdown, and GitLab Pages.

Serves on http://localhost:4000 with:
jekyll serve

New post:
jekyll post "Blog post title"

