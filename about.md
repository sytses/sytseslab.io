---
layout: page
title: About
permalink: /about/
---

I'm Sid Sijbrandij and this website's name comes from my legal first name Sytse.

I like innovative projects and passionate people.
I'm the CEO of GitLab.
I'm married to [Karen Sijbrandij](https://twitter.com/karensijbrandij).

GitLab: [sytses](https://gitlab.com/u/sytses)

Personal email: [website@sytse.com](mailto:website@sytse.com)

Company email: sid@ company domain

Twitter: [@sytses](http://twitter.com/sytses)

LinkedIn: [sijbrandij](http://nl.linkedin.com/in/sijbrandij)

Facebook: [sytse](http://www.facebook.com/sytse)
